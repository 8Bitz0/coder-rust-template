---
name: Rust - Ubuntu Jammy
description: Develop in your browser with code-server, Rust, and Ubuntu 22.04
tags: [local, docker]
---

# Rust - Ubuntu Jammy

Develop in your browser with code-server & the Rust language. Bring your dotfiles with you.

## How to use:

After setting up a workspace, click "Code Server" to open your IDE. You are ready to go!

## "Code Server is offline" error:

It may take a moment for the IDE to go online. If it doesn't, try opening an issue [on the project repository](https://gitlab.com/8Bitz0/coder-rust-template).
