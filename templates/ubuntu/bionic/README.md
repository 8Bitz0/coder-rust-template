---
name: Rust - Ubuntu Bionic
description: Develop in your browser with code-server, Rust, and Ubuntu 18.04
tags: [local, docker]
---

# Rust - Ubuntu Bionic

Develop in your browser with code-server & the Rust language. Bring your dotfiles with you.

## How to use:

After setting up a workspace, click "Code Server" to open your IDE. You are ready to go!

## "Code Server is offline" or "502 Bad Gateway" error:

It may take a moment for the IDE to go online. If it doesn't, try opening an issue [on the project repository](https://gitlab.com/8Bitz0/coder-rust-template).
